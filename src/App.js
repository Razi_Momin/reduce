import React, { useReducer } from 'react';
import ComponentA from './components/ComponentA';
import ComponentB from './components/ComponentB';
import ComponentC from './components/ComponentC';

export const CountContext = React.createContext();

const initialState = { firstCounter: 'default value' };
const reducer = (state, action) => {
  console.log(action);
  // return false;
  switch (action.type) {
    case 'increment':
      return { ...state, firstCounter: action.value }
    case 'decrement':
      return state - 1;
    case 'reset':
      return initialState;
    default:
      return state;
  }
}
function App() {
  // return (<div>k</div>);
  const [count, dispatch] = useReducer(reducer, initialState);
  return (
    <CountContext.Provider value={{ countState: count, countDispatch: dispatch }}>
      <div className="App">
        Change  {count.firstCounter}
        <ComponentA />
        <ComponentB />
        <ComponentC />
      </div>
    </CountContext.Provider>


  );
}

export default App;
