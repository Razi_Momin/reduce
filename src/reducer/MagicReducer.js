// import { v4 as uuidv4 } from 'uuid';
// export const MagicReducer = (state, action) => {
//     // console.log(state);
//     // return false;
//     switch (action.type) {
//         case 'CHANGE_NAME':
//             return [...state, {
//                 name: action.name
//             }]
//         default:
//             return state
//     }
// }


const initialState = { firstCounter: 0 };
export const reducer = (state, action) => {
    console.log(action);
    // return false;
    switch (action.type) {
        case 'increment':
            return { ...state, firstCounter: action.value }
        case 'decrement':
            return state - 1;
        case 'reset':
            return initialState;
        default:
            return state;
    }
}