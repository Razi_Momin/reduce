import React, { createContext, useReducer, useEffect } from 'react'
import { MagicReducer } from '../reducer/MagicReducer';
export const MagicContext = createContext();

const MagicContextProvider = (props) => {
    const [magicData, dispatch] = useReducer(MagicReducer, [], () => {
        let dataArray = {
            name: 'razi',
            age: 28
        }
        return dataArray;
    })
    useEffect(() => {
        // localStorage.setItem('books', JSON.stringify(magicData));
        console.log(magicData);
        magicData.name = 'momin';
        // alert(magicData);
    }, [magicData]);
    return (
        <MagicContext.Provider value={{ magicData, dispatch }}>
            {props.children}
        </MagicContext.Provider>
    )
}
export default MagicContextProvider

