import React, { useContext } from 'react'
import { CountContext } from '../App'

function ComponentD() {
    const countContext = useContext(CountContext)
    return (
        <div>
            Component D
            <button onClick={() => countContext.countDispatch({ type: 'increment', value: 'from D component' })}>Increament</button>
            <button onClick={() => countContext.countDispatch('decrement')}>Decreament</button>
            <button onClick={() => countContext.countDispatch({ type: 'reset' })}>Reset</button>
        </div>
    )
}
export default ComponentD