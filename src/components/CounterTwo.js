import React, { useReducer } from 'react'
const initialState = { firstCounter: 0 };
const reducer = (state, action) => {
    switch (action.type) {
        case 'increment':
            return { ...state, firstCounter: state.firstCounter + action.value }
        case 'decrement':
            return { ...state, firstCounter: state.firstCounter - action.value }
        case 'reset':
            return initialState;
        default:
            return state;
    }
}
export default function CounterTwo() {
    const [count, dispatch] = useReducer(reducer, initialState) //count is define initial value
    return (
        <div>
            <div> Count  - {count.firstCounter}</div>
            <button onClick={() => dispatch({ type: 'increment', value: 1 })}>Increament</button>
            <button onClick={() => dispatch({ type: 'decrement', value: 1 })}>Decreament</button>
            <button onClick={() => dispatch({ type: 'increment', value: 5 })}>Increament 5</button>
            <button onClick={() => dispatch({ type: 'decrement', value: 5 })}>Decreament 5</button>
            <button onClick={() => dispatch({ type: 'reset' })}>Reset</button>
        </div>
    )
}
