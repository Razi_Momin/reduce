import React, { useContext } from 'react'
import { CountContext } from '../App'

function ComponentA() {
    const countContext = useContext(CountContext)
    console.log(countContext);
    const { firstCounter } = countContext.countState;
    return (
        <div>
            Component A {firstCounter}
            <button onClick={() => countContext.countDispatch({ type: 'increment', value: 'from A component' })}>Increament</button>
            <button onClick={() => countContext.countDispatch('decrement')}>Decreament</button>
            <button onClick={() => countContext.countDispatch({ type: 'reset' })}>Reset</button>
        </div>
    )
}
export default ComponentA