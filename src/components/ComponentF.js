import React, { useContext } from 'react'
import { CountContext } from '../App'

function ComponentF() {
    const countContext = useContext(CountContext)
    return (
        <div>
            Component F
            <button onClick={() => countContext.countDispatch({ type: 'increment', value: 'from F component' })}>Increament</button>
            <button onClick={() => countContext.countDispatch('decrement')}>Decreament</button>
            <button onClick={() => countContext.countDispatch({ type: 'reset' })}>Reset</button>
        </div>
    )
}
export default ComponentF